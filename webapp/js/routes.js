'use strict';

angular.module('SeguidorDeSeries.routes', ['ui.router'])
.config(['$stateProvider', '$urlRouterProvider', '$locationProvider', function($stateProvider, $urlRouterProvider, $locationProvider) {
$stateProvider
.state({
  name: 'ej1',
  url: '/ej1',
  templateUrl: 'partials/ej1.html',
  controller: 'SeguidorDeSeriesController',
})
;
$locationProvider.html5Mode({
  enabled: true,
  requireBase: false
});
$urlRouterProvider.otherwise('/ej1');
}]);
