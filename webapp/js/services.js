'use strict';

angular.module("SeguidorDeSeries.services", [])
.service('SeriesService',function(){
	
	var transform = function(json) { return new Serie(json) }
	 
	// Uso un transform para mapear el json a un modelo.
	this.todasLasSeries = function(){
		return this.seriesDB.map(transform);
	}
	
	// Uso un array local para simular la API.
	this.seriesDB = [
	      { "nombre" : "Supernatural", "temporada" : "1", "estado" : "vista" },
	      { "nombre" : "The Flash", "temporada" : "10", "estado" : "mirando" },
	      { "nombre" : "Arrow", "temporada" : "2", "estado" : "pendiente" },
	      { "nombre" : "House of Cards", "temporada" : "3", "estado" : "vista" },
	      { "nombre" : "Game of Thrones", "temporada" : "4", "estado" : "vista" },
	      { "nombre" : "Stranger Things", "temporada" : "1", "estado" : "vista" },
	      { "nombre" : "The Walking Dead", "temporada" : "1", "estado" : "vista" },
	      { "nombre" : "Vikingos", "temporada" : "1", "estado" : "vista" },
	      { "nombre" : "Lucifer", "temporada" : "1", "estado" : "vista" },
	      { "nombre" : "The 100", "temporada" : "1", "estado" : "vista" },
	    ];
	
	// Simulan los cambios de estado
	
	this.cambiar = function(_serie){
		this.seriesDB = this.seriesDB.filter(serie => serie.nombre != _serie.nombre )
		this.seriesDB.push(_serie)
	}
	
});




