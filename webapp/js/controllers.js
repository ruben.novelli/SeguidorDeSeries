'use strict';

angular.module("SeguidorDeSeries.controllers", [])
  .controller('SeguidorDeSeriesController',function(SeriesService){
    /* scope */

    this.textoBusqueda = '';
    
    this.filtroActual = '';
    
    this.serieSeleccionada = null;

    // Uso el filter del ngRepeat para buscar una serie, si se desea hacer busquedas automaticas cambiar en el html el filter por textoBusqueda.
    this.buscar = function() {
        this.filtroActual = this.textoBusqueda;
      };

    // Llamada a un servicio que retorna una lista de series.
  	this.getSeries = function () {
  		this.series = SeriesService.todasLasSeries();
  	};
  	
  	this.getSeries();
    
    this.mostrar = function(serie) {
        this.serieSeleccionada = serie;
    };
    
    // LLamadas al servicio para cambiar el estado de una serie.
    this.cambiarAVista = function(serie) {
    	this.actualizarEstado(serie, "vista");
    };
    
    this.cambiarAPendiente = function(serie) {
    	this.actualizarEstado(serie, "pendiente");
    };
    
    this.cambiarAMirando = function(serie) {
    	this.actualizarEstado(serie, "mirando");
    };
    
    this.actualizarEstado = function(serie, estado) {
    	serie.estado = estado;
    	this.actualizarVistaDeUnaSerie(serie);
	};
    
    this.actualizarVistaDeUnaSerie = function(serie) {
        SeriesService.cambiar(serie); // Para modificar en el backend
    	this.getSeries()
        this.mostrar(serie);
    };
    
    this.haySeleccionada = function() {
		return this.serieSeleccionada !== null;
	};
    
    // Fin de cambios de estado.
});
