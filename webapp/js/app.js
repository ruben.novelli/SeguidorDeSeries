'use strict';

angular.module('SeguidorDeSeries', [
  'SeguidorDeSeries.routes',
  'SeguidorDeSeries.services',
  'SeguidorDeSeries.controllers',
  'SeguidorDeSeries.serie',
]);
