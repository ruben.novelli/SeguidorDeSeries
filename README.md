# Seguidor de Series

## Enunciado

Seguidor de Series

Franco, un chico fanático de las series de TV, sigue muchas series en simultáneo y anda buscando una aplicación donde pueda anotar las series que quiere ver, está viendo o incluso ya vió (que a veces se le olvida).

## Requerimientos
Ver un listado de series mostrando nombre y cantidad de temporadas, identificando cuáles terminó, empezó y están pendientes (puede ser con un color, ícono representativo o texto). 
Permitir una búsqueda por nombre.
Al seleccionar una serie del listado:
Mostrar los datos de la misma al costado
Poder cambiarlas de estado (se puede tener un botón por cada uno, o cualquier otra opción que crea correcta). 

## Instalacion y uso

Para poder correr la aplicacion se requiere utilizar node.js como servidor. 

### Para instalar node
sudo apt-get update
sudo apt-get install nodejs

### Para instalar el gestor npm
sudo apt-get install npm

### Para instalar dependencias
npm install

### Para correr el server, dentro del directorio webapp
node server.js
